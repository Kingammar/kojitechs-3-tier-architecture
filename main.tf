
terraform {
  required_version = ">=1.0.0"

  backend "s3" {
    bucket         = "kojitechs.tf.101.state.bucket"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "path/env"
    encrypt        = "true"

  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}


locals {
  vpc_id = aws_vpc.kojitechs_vpc.id

  azs = data.aws_availability_zones.available.names

}

data "aws_availability_zones" "available" {
  state = "available"
}


# CREATING VPC
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "kojitechs_vpc"
  }
}

# CREATING INTERNET GATEWAY
resource "aws_internet_gateway" "kojitechs_igw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "igw"
  }
}

# CREATING PUBLIC SUBNETS
resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_cidrs) # Telling terraform to calculate the size of public cidr variable
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_cidrs[count.index]
  availability_zone       = element(slice(local.azs, 0, 2), count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"
  }
}

# CREATING PRIVATE SUBNET
resource "aws_subnet" "private_subnets" {
  count             = length(var.private_cidrs) # Telling terraform to calculate the size of private cidr variable
  vpc_id            = local.vpc_id
  cidr_block        = var.private_cidrs[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index)


  tags = {
    Name = "private_subnet_${count.index + 1}"
  }
}

# CREATING DATABASE SUBNET
resource "aws_subnet" "database_subnets" {
  count             = length(var.database_cidrs) # Telling terraform to calculate the size of database cidr variable
  vpc_id            = local.vpc_id
  cidr_block        = var.database_cidrs[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index)


  tags = {
    Name = "database_subnet_${count.index + 1}"
  }
}

# CREATING PUBLIC ROUTE TABLE
resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.kojitechs_igw.id
  }
  tags = {
    Name = "public_route_table"
  }
}
# CREATING ROUTE TABLE ASSOCIATION FOR PUBLIC SUBNET
resource "aws_route_table_association" "public_association" {
  count          = length(var.public_cidrs)
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

# CREATING DEFAULT ROUTE TABLE
resource "aws_default_route_table" "default_route_table" {
  default_route_table_id = aws_vpc.kojitechs_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
   gateway_id = aws_nat_gateway.kojitechs_nat.id
  }
}

#gateway_id = aws_internet_gateway.kojitechs_nat.id

# CREATING NAT GATEWAY
resource "aws_nat_gateway" "kojitechs_nat" {
  allocation_id = aws_eip.kojitechs_eip.id  # IT WOULDN'T MAKE SENSE TO HAVE A DYNAMIC ADDRESS ON A NAT DEVICE
  subnet_id     = aws_subnet.public_subnets[0].id   # PLACE IN THE PUBLIC SUBNET

  tags = {
    Name = "gw NAT"
  }
  depends_on = [aws_internet_gateway.kojitechs_igw]
}

# CREATING ELASTIC IP
resource "aws_eip" "kojitechs_eip" {
  # instance = aws_instance.web.id
  vpc      = true
  depends_on = [aws_internet_gateway.kojitechs_igw]
}





