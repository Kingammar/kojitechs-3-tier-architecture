
variable "vpc_cidr" {
  description = "vpc cidr"
  type        = string
  default     = "10.0.0.0/16"

}

variable "public_cidrs" {
  description = "public subnet cidr"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24"]

}

variable "private_cidrs" {
  description = "private subnet cidr"
  type        = list(any)
  default     = ["10.0.10.0/24", "10.0.3.0/24"]

}

variable "database_cidrs" {
  description = "database subnet cidr"
  type        = list(any)
  default     = ["10.0.6.0/24", "10.0.55.0/24"]

}