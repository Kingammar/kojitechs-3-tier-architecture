
resource "aws_iam_role" "ssm_fleet_ec2" {
  name = "registration_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "ssm_fleet_ec2"
  }
}

# TO REFERENCE THIS TO EC2, WE HAVE TO PARSE ==> aws_iam_instance_profile.instance_profile in ec2
resource "aws_iam_instance_profile" "instance_profile" {
  name = "registration_profile"
  role = aws_iam_role.ssm_fleet_ec2.name
}

# resource "aws_iam_policy" "policy" {
#   name = "registration_policy"
#   description = "access  policy of ec2 to ssm fleet"
#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#          "ssm:UpdateInstanceInformation",
#           "ssmmessages:CreateControlChannel",
#           "ssmmessages:CreateDataChannel",
#           "ssmmessages:OpenControlChannel",
#           "ssmmessages:OpenDataChannel"
#       ],
#       "Resource": "*",
#       "Effect": "Allow"
#     }   
#   ]
# }
# EOF
# }

